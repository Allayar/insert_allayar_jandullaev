-- add film
insert into film (title, description,  release_year, language_id, rental_duration, rental_rate)
values('INTERSTELLAR','Interstellar is a 2014 epic science fiction film co-written, directed, 
and produced by Christopher Nolan. It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, 
Bill Irwin, Ellen Burstyn, Matt Damon, and Michael Caine. Set in a dystopian future where humanity is 
embroiled in a catastrophic blight and famine, the film follows a group of astronauts who travel through 
a wormhole near Saturn in search of a new home for humankind.',  2014, 1, 14, 4.99)    

-- check your result
select *
from film f
where f.title = 'INTERSTELLAR';


-- add actors
insert into actor (first_name, last_name)
values ( 'MATTHEW','MCCONAUGHEY'),
        ('ANNE','HATHAWAY'),
        ('JESSICA','CHASTAIN'),
        ('BILL','IRWIN'),
        ('MICHEAL','CAINE');


-- add film_actor
insert into film_actor (actor_id, film_id)
values (201, 1001),
       (202, 1001),
       (203, 1001),
       (204, 1001),
       (205, 1001);

-- check your result
select * 
from film_actor fa
where fa.actor_id > 200



-- add inventory
insert into inventory (film_id, store_id)
values(1001, 1),
      (1001, 2)

-- check your result
select * 
from inventory i 
where film_id = 1001
